package com.example.myapplication;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {
    TextView tvdisplay;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        initViewReference();
        dispalyDataSecondPage();
    }

    private void dispalyDataSecondPage() {
        String value = getIntent().getStringExtra("passingname");
        tvdisplay.setText(value);
    }


    void initViewReference() {
        tvdisplay = findViewById(R.id.tvactDisplay);
    }


}
