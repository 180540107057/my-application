package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    EditText etFirstname, etLastname;
    Button btnSubmit, btncalculator;
    TextView tvDisplay;
    ImageView ivClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etFirstname = findViewById(R.id.etactFirstname);
        etLastname = findViewById(R.id.etactSecondname);
      btnSubmit = findViewById(R.id.btnactSubmit);
      tvDisplay = findViewById(R.id.tvactDisplay);
        ivClose = findViewById(R.id.ivActClose);
        btncalculator = findViewById(R.id.btActCalculator);

      btnSubmit.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              String FirstName = etFirstname.getText().toString();
              String SecondName = etLastname.getText().toString();
              String temp = FirstName + " " + SecondName;
              Intent intent1 = new Intent(MainActivity.this, SecondActivity.class);
              String concatnames = "Your Full Name Is:" + "\n" + etFirstname.getText().toString() + " "
                      + etLastname.getText().toString();
              intent1.putExtra("passingname", concatnames);
              startActivity(intent1);


          }
      });
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btncalculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CalculatorActivity.class);
                startActivity(intent);
            }
        });

    }

}